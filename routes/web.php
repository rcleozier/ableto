<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::middleware('auth')->get('/dashboard', 'DashboardController@index')->name('home');

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->get('/questions', 'QuestionController@index');
Route::middleware('auth')->get('/answers', 'AnswersController@index');
Route::middleware('auth')->post('/answers', 'AnswersController@store');

var Dashboard = function() {

  this.init = function() {
      this.attachedListeners();
      this.getQuestions();
      this.getPastResponses();
  };

  this.attachedListeners = function() {
    var that = this;
    $("#main-questions__save").on('click', function() {
        that.saveResponse();
    });
  };

  this.saveResponse = function() {
    var posts = [];
    $(".question").each(function(element) {
        var questionId = $(this).attr("data-question-id");
        var answerId = $(this).find('option:selected').attr('data-answer-id');

        var post = {
          question: questionId,
          answer: answerId
        };

        posts.push(post);
    });

    this.sendSaveResponse(posts);
  };

  this.sendSaveResponse = function(posts) {
      var that = this;

      axios.post('/answers', {data: posts})
      .then((response)=>{
          if(response.data.status =='ok'){
              that.getPastResponses();
          }
      });
  };

  this.getPastResponses = function() {
      axios.get('/answers')
          .then((response)=>{
            if(response.data.status =='ok'){
                // Clear container
                $('#main-responses').html('');
                response.data.data.forEach(function(answer) {
                  var content = answer.question.text + " : " + answer.answer.text + " " + answer.created_at;
                  $('#main-responses').append(
                      $('<div/>')
                      .append("<span/>")
                      .text(content )
                  );
                });
            }
          });

  };

  this.getQuestions = function() {
      axios.get('/questions')
      .then((response)=>{
        if(response.data.status =='ok'){
          response.data.data.forEach(function(question) {
              var questionId = question.id;
              var answerClassId = "answer_" + questionId;
              var questionClassId = "question_" + questionId;

              $('#main-questions').append(
                $('<div/>')
                  .attr("class", questionClassId + " question")
                  .attr("data-question-id", questionId)
                  .append("<span/>")
                    .text(question.text)
              );

              var select = $('<select />').attr("class", answerClassId);
              for(var val in question.answers) {
                $('<option />', {
                    'data-answer-id' : question.answers[val].id,
                    value: val,
                    text: question.answers[val].text
                }).appendTo(select);
              }
              $("." + questionClassId).append(select);
          });
        }
      })
  };

};

var app = new Dashboard();
app.init();

<div id="app-questions">

    <div>Please tell us about your current day:</div>
    <ul>
        <li v-for="question in questions">
            <div>{{question.text}} </div>
            <select v-model="user_answers[question.id]">
                <option v-for="answers in question.answers" v-bind:value="answers.value">{{answers.text}}</option>
            </select>
        </li>
    </ul>

    <button v-on:click="save">Save</button>
    <button v-on:click="getNewQuestions">Reset</button>

    <div>Previous Answers:</div>
    <ul>
        <li v-for="(value, key) in previousAnswers">
            <div>{{formatTime(previousAnswers[key].created_at)}}  {{previousAnswers[key].question.text}}: {{previousAnswers[key].answer.text}}</div>
        </li>
    </ul>




</div>

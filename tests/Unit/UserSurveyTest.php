<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\UserSurvey;
use App\User;

class UserSurveyTest extends TestCase
{

    use DatabaseMigrations;

    private $user_survey_service;
    /**
     * A basic test example.
     *
     * @return void
     */

    public function setUp()
    {
        parent::setUp();
        $this->seed('DatabaseSeeder');
        $this->user_survey_service = new UserSurvey();
    }


    public function testGetQuestions()
    {
        $questions = $this->user_survey_service->getQuestions();
        $this->assertEquals(5,count($questions));
        $questions_2 = $this->user_survey_service->getQuestions();
        $this->assertTrue($questions[0]['id'] !== $questions_2[0]['id']);
    }

    public function testGetPreviousAnswers()
    {
        $answers = $this->user_survey_service->getPreviousAnswers(3);
        $this->assertEquals(0,count($answers));
    }

    public function testSaveAnswers(){
        $user = new User;
        $user->name = 'Test User';
        $user->email = 'testUser@abletotest.com';
        $user->password = 'abc1123';
        $user->save();
        $user_id = $user->id;

        $questions = $this->user_survey_service->getQuestions();
        $question_1_id = $questions[0]['id'];
        $answer_1_id = $questions[0]['answers'][1]['id'];
        $question_2_id = $questions[1]['id'];
        $answer_2_id = $questions[1]['answers'][0]['id'];

        $q_a_Array = [
            ['question_id'=>$question_1_id,'answer_id'=>$answer_1_id],
            ['question_id'=>$question_2_id,'answer_id'=>$answer_2_id],
        ];

        $this->assertTrue($this->user_survey_service->saveAnswers($q_a_Array,$user_id));
    
        $answers = $this->user_survey_service->getPreviousAnswers($user_id);

        $this->assertEquals(2,count($answers));
    }
}

<?php
/**
 * This service assist with retrieving questions and answers from the DB
 */

namespace App\Services;

use App\UserResponse;
use App\User;
use App\Question;

class QuestionsAnswers
{

    /**
     * Get all questions
     * @param null
     * @return array
     */
    public function getQuestions() : array
    {
        $questions = Question::with('answers')->get();
        return $this->groupAnswersAndQuestions($questions->toArray());
    }

    /**
     * Gets a collection of questions answered by a user
     * @param int user_id
     * @return Collection
     */
    public function getUserResponses($user_id)
    {
        return UserResponse::with('question','answer')->where('user_id', $user_id)->get();
    }

    /**
     * Saves user response
     * @param array question_id,answer_id
     * @param int user_id
     * @return bool
     */
    public function saveResponse($userResponse, $user_id)
    {
      foreach ($userResponse as $key => $value) {
          $userQuestions = new UserResponse;
          $userQuestions->question_id = $value['question'];
          $userQuestions->answer_id = $value['answer'];
          $userQuestions->user_id = $user_id;
          $userQuestions->save();
      }

      return true;
    }

    private function groupAnswersAndQuestions(array $array_of_elements): array
    {
        shuffle($array_of_elements);
        return array_slice($array_of_elements, 0,5);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['text','created_at','updated_at'];

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }
}

<?php
/**
 * Controller responsible for Questions
 */
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use App\Services\QuestionsAnswers;

class QuestionController extends Controller {

    public function __construct(QuestionsAnswers $questionsAnswersService)
    {
      $this->questionsAnswersService = $questionsAnswersService;
    }

   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
   public function index() {
      return Response::json(array(
        'status'=>'ok',
        'response'=>'',
        'data'=>$this->questionsAnswersService->getQuestions()
      ));
   }

}

<?php
/**
 * Controller responsible for Answers
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Services\QuestionsAnswers;

class AnswersController extends Controller
{
    public function __construct(QuestionsAnswers $questionAnswersService)
    {
      $this->questionAnswersService = $questionAnswersService;
    }

    /**
     * Get all user responses based on Logged in user
     *
     * @return Response
     */
    public function index() {
        $previousResponses = $this->questionAnswersService->getUserResponses(
          Auth::user()->id
        );

        $response = array(
          'status'=>'ok',
          'data'=> $previousResponses
        );

        return Response::json($response);
   }

   /**
    * Store response to questions in DB
    *
    * @return Response
    */
   public function store(Request $request) {

      $userResponse = $request->get('data');

      if(!$userResponse || !is_array($userResponse)){
        return Response::json(array(
          'status'=>'err',
          'response'=>''));
      }

      $isSaved = $this->questionAnswersService->saveResponse(
          $userResponse,
          Auth::user()->id
      );

      if($isSaved){
        $response = array(
          'status'=>'ok',
          'response'=>'saved!',
          'data'=>$this->questionAnswersService->getUserResponses(Auth::user()->id));
      }else{
        $response = array(
          'status'=>'err',
          'response'=>'Could not save response');
      }

      return Response::json($response);
   }

}

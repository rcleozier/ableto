<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserResponse extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','question_id', 'answer_id','created_at','updated_at'
    ];

    public $timestamps = true;

    public function question()
    {
        return $this->belongsTo('App\Question','question_id');
    }

    public function answer()
    {
        return $this->belongsTo('App\Answer','answer_id');
    }
}

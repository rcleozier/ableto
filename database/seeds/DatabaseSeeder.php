<?php
// Robbins Cleozier

use Illuminate\Database\Seeder;
use App\Question;
use App\Answer;

class DatabaseSeeder extends Seeder
{
    private $questionsAnswers =  [
        [
            'question'=>'Are you feeling any pain today',
            'answers'=>['Yes', 'No', 'Slightly']
        ],
        [
            'question'=>'How would you describe your enery level',
            'answers'=>['High', 'Medium', 'Low']
        ],
        [
            'question'=>'How are you feeling?',
            'answers'=>['Great', 'Good', 'Bad']
        ],
        [
            'question'=>'Did you enjoy work today',
            'answers'=>['Yes', 'No']
        ],
        [
            'question'=>'Whats your favorite meal',
            'answers'=>['Breakfast', 'Lunch', 'Dinner']
        ],
        [
            'question'=>'Do you like any of the following foods?',
            'answers'=>['Pasta', 'Chicken Wings', 'Steak', 'None']
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      foreach ($this->questionsAnswers as $key => $value) {
          $question = new Question;
          $question->text = $value['question'];
          $question->save();
          $id = $question->id;

          foreach ($value['answers']as $key => $value) {
              $answer = new Answer;
              $answer->text = $value;
              $answer->question_id = $id;
              $answer->save();
          }
       }
    }
}

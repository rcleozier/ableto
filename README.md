### ReadMe

Test app built using Laravel 5 and Jquery/Javascript


## Installing
```
- git clone https://github.com/rcleozier/sampleapp.git ./
- cd into sampleapp directory
- composer install;
```

## Getting the Data
```
- php artisan migrate
- php artisan db:seed
```

## Demo App

https://fierce-ocean-81263.herokuapp.com/dashboard
